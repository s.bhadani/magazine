class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :article
  has_many   :replies, class_name: "Comment", foreign_key: "parent"
  belongs_to :reply_comment, class_name: "Customer"
end

